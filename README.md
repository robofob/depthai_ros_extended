# 1. DepthAI ROS Extended
OAK-D series cameras by [Luxonis](https://www.luxonis.com/) are great! But robotics developing often comes with computer simulation and the Gazebo is a widespread tool for it. It is very handy to have "Gazebo avatar" of hardware that real robot uses. This package provides extended urdf for OAK-D camera, which contains needed Gazebo plugins. Also with this package is possible to use real camera to extract depth from two mono images from Gazebo simulator. This is helps to test robot software on data which is more close to reality, as well to obtain data from simulator for training stuff. Also this package provides universal (so-so but I do my best) node for real camera.

## 1.1. Install
Package is made and tested with ROS Noetic.  
1. Install official DepthAI [ROS package](https://github.com/luxonis/depthai-ros) of OAK-D series cameras.
2. Clone this repository in your ROS workspace and build it.

# 2. oakd_gazebo_macro.urdf.xacro
This macro is based on original [OAK-D urdf](https://github.com/luxonis/depthai-ros/tree/ros-release/depthai_bridge/urdf) and has some additional features:
 - [X] Simulation of RGB camera with different resolution supported
 - [X] Simulation of Depth stream with different resolution supported
 - [X] Simulation of Rigth and Left mono cameras (can be switched off)
 - [X] Possibility to add tf_prefix for all camera frames
 - [x] IMU simulation

## 2.1. Topics structure
```mermaid
graph TD
    Main(namespace/camera_name)
    Main --> RGB(rgb)
    RGB --> rgb_i(image)
    RGB --> rgb_ci(camera_info)
    Main --> depth(depth)
    depth --> depth_i(image)
    depth --> depth_ci(camera_info)
    Main --> right(right)
    Main --> left(left)
    right --> rigth_i(image)
    right --> right_ic(camera_info)
    left --> left_i(image)
    left --> left_ci(camera_info)
    Main --> net(net)
    net --> raw_output(raw_output)
    net --> preview(preview)
    preview --> preview_i(image)
    preview --> preview_ci(camera_info)
    Main --> imu(imu)
```
Namespace `net` is for neural net onboard processing. Preview is an image scaled to the given inputs. `raw_output` is output neural network tensor.

## 2.2. TF frames structure
```mermaid
graph TD
    pl(parent link)
    pl --> main(tf_prefix/camera_name)
    main --> left(tf_prefix/camera_name_left_camera_frame)
    left --> left_opt(tf_prefix/camera_name_left_camera_optical_frame)
    main --> right(tf_prefix/camera_name_right_camera_frame)
    right --> right_opt(tf_prefix/camera_name_right_camera_optical_frame)
    main --> rgb(tf_prefix/camera_name_rgb_camera_frame)
    rgb --> rgb_opt(tf_prefix/camera_name_rgb_camera_optical_frame)
    pl --> imu_frame(tf_prefix/imu_frame)
```

## 2.3. Supported camera resolutions
More information you can find on official depthai pages ([OAK-D](https://docs.luxonis.com/projects/hardware/en/latest/pages/BW1098OAK.html) & [OAK-D-LITE](https://docs.luxonis.com/projects/hardware/en/latest/pages/DM9095.html)). 
| Resolution Name | Width | Height | OAK-D RGB | OAK-D Mono&Depth | OAK-D-LITE RGB | OAK-D-LITE Mono&Depth |
|-----------------|-------|--------|-----------|------------------|----------------|-----------------------|
| 13mp            | 4208  | 3120   | -         | -                | +              | -                     |
| 12mp            | 4056  | 3040   | +         | -                | +              | -                     |
| 4k              | 3840  | 2160   | +         | -                | +              | -                     |
| 1080p           | 1920  | 1080   | +         | -                | +              | -                     |
| 800p            | 1280  | 800    | -         | +                | -              | -                     |
| 720p            | 1280  | 720    | *         | +                | *              | -                     |
| 480p            | 640   | 480    | -         | -                | -              | +                     |
| 400p            | 640   | 400    | -         | +                | -              | +                     |
| 360p            | 640   | 360    | *         | -                | *              | -                     |
 - \+ - supported  
 - \- - notsupported
 - \* - supported through resizing

# 3. universal_node
As soon as there are some numbers of [different nodes](https://github.com/luxonis/depthai-ros/tree/ros-release/depthai_examples/ros1_src) for OAK-D cameras for ROS, this package provides own node in respect to provided urdf.

## 3.1. Params
 - __~tf_prefix__ (string, default: "") tf_prefix for frame_id fields in output topics
 - __~camera_name__ (string, default: oakd) name of camera used in topics and frames (see upper)
 - __~lrcheck__ (bool, default: true) see depthai documentation for more info
 - __~extended__ (bool, default: false) see depthai documentation for more info
 - __~subpixel__ (bool, default: false) see depthai documentation for more info
 - __~confidence__ (int, default: 200) see depthai documentation for more info
 - __~LRchecktresh__ (int, default: 5) see depthai documentation for more info
 - __~monoResolution__ (string, default: 720p) resolution for mono and depth streams (see variants upper)
 - __~rgbResolution__ (string, default: 1080p) resolution for color stream (see variants upper)
 - __~outputMono__ (bool, default: false) If true publishes also images from Left and Right cameras
 - __~enableIMU__ (bool, default: false) If true enables IMU module (not on OAK-D-Lite cam)
 - __~imuMode__ (int, default: 0) TODO link to luxonic doc
 - __~linearAccelCovariance__ (double, default: 0.0) Manual set of linear acceleration cov values for accelerometer
 - __~angularVelCovariance__ (double, default: 0.0) Manual set of angalar velocity cov values for hyroscope
 - __~NNblobPath__ (string, default: "") If set, loads neural net to the camera, please read luxonis documentations for formats and restrictions
 - __~netInputWidth__ (int, default: 0) If __~NNblobPath__ is set. Net input size.
 - __~netInputHeight__ (int, default: 0) If __~NNblobPath__ is set. Net input size.

Some info of dpethai inherited params can be obtainen [here](https://docs.luxonis.com/projects/sdk/en/latest/api/#depthai_sdk.managers.PipelineManager.createDepth).

## 4. depth_extractor_node.py
Scripts runs OAK-D camera and sends on it right and left images form topic, recieves depth image and publishes it further.

## 4.1. Params
 - __~lrcheck__ (bool, default: true) see depthai documentation for more info
 - __~extended__ (bool, default: false) see depthai documentation for more info
 - __~subpixel__ (bool, default: false) see depthai documentation for more info

## 4.2. Subscribed topics
 - __left__ ([sensor_msgs/Image](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/Image.html)) Image from Left camera
 - __right__ ([sensor_msgs/Image](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/Image.html)) Image from Right camera
 - __right/info__ ([sensor_msgs/CameraInfo](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/CameraInfo.html)) Info topic from right camera

## 4.3. Published topics
 - __~depth__ ([sensor_msgs/Image](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/Image.html)) Extracted depth (mobo16 encoding)
 - __~camera_info__ ([sensor_msgs/CameraInfo](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/CameraInfo.html)) Same topic as __right/info__, but synchronized in time with __~depth__
