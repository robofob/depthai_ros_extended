/*
 * ROS node for OAK-D camera series
 * Author: Moscowsky Anton
 */

#include "ros/ros.h"

#include <iostream>
#include <cstdio>
#include <tuple>
#include "sensor_msgs/Image.h"
#include "stereo_msgs/DisparityImage.h"
#include <camera_info_manager/camera_info_manager.h>
#include "sensor_msgs/Imu.h"
#include <functional>

#include "depthai/depthai.hpp"
#include <depthai_bridge/BridgePublisher.hpp>
#include <depthai_bridge/ImageConverter.hpp>
#include <depthai_bridge/DisparityConverter.hpp>
#include "depthai/pipeline/node/IMU.hpp"
#include "depthai_bridge/ImuConverter.hpp"
#include "depthai_ros_extended_msgs/NeuralNetworkRawOutput.h"
#include "depthai_ros_extended_msgs/LayerRawOutput.h"
#include "std_msgs/MultiArrayDimension.h"

#include "NNDataRawConverter.h"

std::tuple<dai::Pipeline, int, int, int, int> createPipeline(int fps, bool enableDepth, bool enableMono, bool lrcheck, bool extended, bool subpixel, int confidence, int LRchecktresh, std::string colorResolution_, std::string monoResolution_, std::string NNblobPath, int netInputWidth, int netInputHeight, bool enableIMU){
    dai::Pipeline pipeline;
    dai::node::ColorCamera::Properties::SensorResolution colorResolution;
    
    dai::node::MonoCamera::Properties::SensorResolution monoResolution; 
    
    std::shared_ptr<dai::node::MonoCamera> monoLeft;
    std::shared_ptr<dai::node::MonoCamera> monoRight;
    if( enableDepth || enableMono){
        monoLeft = pipeline.create<dai::node::MonoCamera>();
        monoRight = pipeline.create<dai::node::MonoCamera>();
    }
    
    
    std::shared_ptr<dai::node::XLinkOut> xoutLeft;
    std::shared_ptr<dai::node::XLinkOut> xoutRight;
    if( enableMono ){
        xoutLeft    = pipeline.create<dai::node::XLinkOut>();
        xoutRight   = pipeline.create<dai::node::XLinkOut>();    
    }
        
//     auto stereo      = pipeline.create<dai::node::StereoDepth>();
//     auto xoutDepth   = pipeline.create<dai::node::XLinkOut>();
    std::shared_ptr<dai::node::StereoDepth> stereo;
    std::shared_ptr<dai::node::XLinkOut> xoutDepth;
    if( enableDepth ){
        stereo = pipeline.create<dai::node::StereoDepth>();
        xoutDepth = pipeline.create<dai::node::XLinkOut>();
    }
    
    // IMU
    std::shared_ptr<dai::node::XLinkOut> xoutImu;
    std::shared_ptr<dai::node::IMU> imu;
    if( enableIMU ){
        xoutImu = pipeline.create<dai::node::XLinkOut>();
        xoutImu->setStreamName("imu");
        imu = pipeline.create<dai::node::IMU>();
        
        imu->enableIMUSensor(dai::IMUSensor::ACCELEROMETER_RAW, 500);
        imu->enableIMUSensor(dai::IMUSensor::GYROSCOPE_RAW, 400);
        //imu->enableIMUSensor(dai::IMUSensor::MAGNETOMETER_RAW, 400);
        imu->setBatchReportThreshold(5);
        imu->setMaxBatchReports(20);  // Get one message only for now.
    }

    auto colorCam = pipeline.create<dai::node::ColorCamera>();
    auto xoutRGB = pipeline.create<dai::node::XLinkOut>();
    
    // XLinkOut    
    if( enableMono ){
        xoutLeft->setStreamName("left");
        xoutRight->setStreamName("right");    
    }
    
    if( enableDepth ){
        xoutDepth->setStreamName("depth");        
    }
    xoutRGB->setStreamName("video");
    
    int rgbScaleNumerator = 1, rgbScaleDinominator = 1;
    
    int colorWidth, colorHeight;    
    if(colorResolution_ == "13mp"){
        colorResolution = dai::node::ColorCamera::Properties::SensorResolution::THE_13_MP;
        colorWidth = 4208;
        colorHeight = 3120;    
    }
    else if(colorResolution_ == "12mp"){
        colorResolution = dai::node::ColorCamera::Properties::SensorResolution::THE_12_MP;
        colorWidth = 4056;
        colorHeight = 3040;        
    }
    else if(colorResolution_ == "4k"){
        colorResolution = dai::node::ColorCamera::Properties::SensorResolution::THE_4_K;
        colorWidth = 3840;
        colorHeight = 2160;        
    }    
    else if(colorResolution_ == "1080p"){
        colorResolution = dai::node::ColorCamera::Properties::SensorResolution::THE_1080_P;
        colorWidth = 1920;
        colorHeight = 1080;
    }
//     else if(colorResolution_ == "800p"){
//         colorResolution = dai::node::ColorCamera::Properties::SensorResolution::THE_800_P;
//         colorWidth = 1280;
//         colorHeight = 800;      
//     }
    else if(colorResolution_ == "720p"){
        // basicly unsupported by OAK-D and OAK-D-LITE
        colorResolution = dai::node::ColorCamera::Properties::SensorResolution::THE_1080_P;
        colorWidth = 1280;
        colorHeight = 720;        
        rgbScaleNumerator = 3;
        rgbScaleDinominator = 2;
    } 
    else if(colorResolution_ == "360p"){
        // basicly unsupported by OAK-D and OAK-D-LITE
        colorResolution = dai::node::ColorCamera::Properties::SensorResolution::THE_1080_P;
        colorWidth = 640;
        colorHeight = 360;        
        rgbScaleNumerator = 1;
        rgbScaleDinominator = 3;
    } 
    else{
        ROS_ERROR("Invalid parameter. -> rgbResolution: %s", colorResolution_.c_str());
        throw std::runtime_error("Invalid RGB camera resolution.");
    }    

    int monoWidth, monoHeight;    
    if(monoResolution_ == "720p"){
        monoResolution = dai::node::MonoCamera::Properties::SensorResolution::THE_720_P; 
        monoWidth  = 1280;
        monoHeight = 720;
    }else if(monoResolution_ == "400p" ){
        monoResolution = dai::node::MonoCamera::Properties::SensorResolution::THE_400_P; 
        monoWidth  = 640;
        monoHeight = 400;
    }else if(monoResolution_ == "800p" ){
        monoResolution = dai::node::MonoCamera::Properties::SensorResolution::THE_800_P; 
        monoWidth  = 1280;
        monoHeight = 800;
    }else if(monoResolution_ == "480p" ){
        monoResolution = dai::node::MonoCamera::Properties::SensorResolution::THE_480_P; 
        monoWidth  = 640;
        monoHeight = 480;
    }else{
        ROS_ERROR("Invalid parameter. -> monoResolution: %s", monoResolution_.c_str());
        throw std::runtime_error("Invalid mono camera resolution.");
    }
        
        
    // ColorCamera    
    colorCam->setBoardSocket(dai::CameraBoardSocket::RGB);
    colorCam->setFps(fps);
    colorCam->setResolution(colorResolution);
    colorCam->setInterleaved(true);   
    if( rgbScaleDinominator != 1 or rgbScaleNumerator != 1){
        colorCam->setIspScale(rgbScaleNumerator, rgbScaleDinominator);
    }
        
    // MonoCamera
    if( enableDepth || enableMono ){
        monoLeft->setResolution(monoResolution);
        monoLeft->setBoardSocket(dai::CameraBoardSocket::LEFT);
        monoLeft->setFps(fps);
        monoRight->setResolution(monoResolution);
        monoRight->setBoardSocket(dai::CameraBoardSocket::RIGHT);
        monoRight->setFps(fps);
    }
    
    // StereoDepth
    if( enableDepth ){
        stereo->initialConfig.setConfidenceThreshold(confidence);
        stereo->setRectifyEdgeFillColor(0); // black, to better see the cutout
        stereo->initialConfig.setLeftRightCheckThreshold(LRchecktresh);
        stereo->setLeftRightCheck(lrcheck);
        stereo->setExtendedDisparity(extended);
        stereo->setSubpixel(subpixel);
    }
    
    // NN
    std::shared_ptr<dai::node::NeuralNetwork> neuralNetwork;
    if( ! NNblobPath.empty() ){        
        neuralNetwork = pipeline.create<dai::node::NeuralNetwork>();
        neuralNetwork->setBlobPath(NNblobPath);                        
        // Color Preview
        colorCam->setPreviewSize(netInputWidth, netInputHeight);   
        colorCam->setInterleaved(false);
        colorCam->setColorOrder(dai::ColorCameraProperties::ColorOrder::BGR);
    }

    // Link plugins CAM -> STEREO -> XLINK    
    if( enableDepth ){
        monoLeft->out.link(stereo->left);
        monoRight->out.link(stereo->right);
    }    
    if( enableMono ){
        stereo->syncedLeft.link(xoutLeft->input);
        stereo->syncedRight.link(xoutRight->input);    
    }            
    if( enableDepth )
        stereo->depth.link(xoutDepth->input);


    if( rgbScaleDinominator != 1 or rgbScaleNumerator != 1){
        colorCam->isp.link(xoutRGB->input);
    }
    else{
        colorCam->video.link(xoutRGB->input);
    }
    // IMU
    if( enableIMU ){
        imu->out.link(xoutImu->input);
    }
    
    // CAM -> PREVIEW -> NN
    if( ! NNblobPath.empty() ){
        auto colorPreview = pipeline.create<dai::node::XLinkOut>();
        colorPreview->setStreamName("preview");        
        //colorCam->preview.link(colorPreview->input);   
        neuralNetwork->passthrough.link(colorPreview->input);
        colorCam->preview.link(neuralNetwork->input);
        
        auto nn_out = pipeline.create<dai::node::XLinkOut>();
        nn_out->setStreamName("nn_out_raw");
        neuralNetwork->out.link(nn_out->input);
                
    }
                 

    return std::make_tuple(pipeline, colorWidth, colorHeight, monoWidth, monoHeight);
}

int main(int argc, char** argv){

    ros::init(argc, argv, "stereo_node");
    ros::NodeHandle pnh("~");
    
    std::string tfPrefix = "", camera_name = "oakd";    
    std::string cameraParamUri;
    int badParams = 0;
    bool enableDepth = true;
    bool lrcheck = true, extended = false, subpixel = false;
    int confidence = 200;
    int rgbWidth, rgbHeight;
    int monoWidth, monoHeight;
    int LRchecktresh = 5;
    std::string rgbResolution = "1080p";
    std::string monoResolution = "720p";
    dai::Pipeline pipeline;
    bool enableMono = false;
    bool depthAligned = true;
    int queueSize = 30;
    int fps = 30;
    
    std::string NNblobPath = "";
    int netInputWidth = 0, netInputHeight = 0;

    badParams += !pnh.getParam("camera_param_uri", cameraParamUri);
    pnh.getParam("tf_prefix",        tfPrefix);    
    pnh.getParam("camera_name",        camera_name);
    pnh.getParam("enableDepth",          enableDepth);
    pnh.getParam("lrcheck",          lrcheck);
    pnh.getParam("extended",         extended);
    pnh.getParam("subpixel",         subpixel);
    pnh.getParam("confidence",       confidence);
    pnh.getParam("LRchecktresh",     LRchecktresh);
    pnh.getParam("monoResolution",   monoResolution);
    pnh.getParam("rgbResolution",   rgbResolution);
    pnh.getParam("outputMono",   enableMono);
    pnh.getParam("depthAligned",   depthAligned);
    pnh.getParam("queueSize",   queueSize);
    pnh.getParam("fps",   fps);
     
    // IMU
    bool enableIMU = false;
    int imuModeParam = 0;    
    double linearAccelCovariance = 0, angularVelCovariance = 0;
    pnh.getParam("enableIMU", enableIMU);    
    pnh.getParam("imuMode", imuModeParam);
    dai::ros::ImuSyncMethod imuMode = static_cast<dai::ros::ImuSyncMethod>(imuModeParam);
    pnh.getParam("linearAccelCovariance", linearAccelCovariance);
    pnh.getParam("angularVelCovariance", angularVelCovariance);
    
    // NN stuff
    pnh.getParam("NNblobPath",   NNblobPath);
    ros::Publisher nn_raw_output_pub;
    if( !NNblobPath.empty()){
        pnh.getParam("netInputWidth",   netInputWidth);
        pnh.getParam("netInputHeight",   netInputHeight);
        
        nn_raw_output_pub = pnh.advertise<depthai_ros_extended_msgs::NeuralNetworkRawOutput>("net/raw_output",1);
    }
    
    
    if (badParams > 0)
    {   
        std::cout << " Bad parameters -> " << badParams << std::endl;
        throw std::runtime_error("Couldn't find %d of the parameters");
    }
    
    std::string full_tf_prefix;
    if( tfPrefix != "" )        
        full_tf_prefix = tfPrefix + "/" + camera_name;
    else
        full_tf_prefix = camera_name;
    

    std::tie(pipeline, rgbWidth, rgbHeight, monoWidth, monoHeight) = createPipeline(fps, enableDepth, enableMono, lrcheck, extended, subpixel, confidence, LRchecktresh, rgbResolution, monoResolution, NNblobPath, netInputWidth, netInputHeight, enableIMU);

    dai::Device device(pipeline);
        
    auto colorQueue = device.getOutputQueue("video", queueSize, false);

    auto calibrationHandler = device.readCalibration();

    auto boardName = calibrationHandler.getEepromData().boardName;
    if( boardName == "OAK-D-LITE"){
        if (monoHeight > 480) {
            monoWidth = 640;
            monoHeight = 480;
        }        
        if( enableIMU ){
            ROS_WARN("IMU was disabled, on OAK-D-LITE!");
            enableIMU = false;
        }
    }
                    
    
    dai::ros::ImageConverter rgbConverter(full_tf_prefix + "_rgb_camera_optical_frame", false);
    auto rgbCameraInfo = rgbConverter.calibrationToCameraInfo(calibrationHandler, dai::CameraBoardSocket::RGB, rgbWidth, rgbHeight); 
    dai::rosBridge::BridgePublisher<sensor_msgs::Image, dai::ImgFrame> rgbPublish(colorQueue,
                                                                                pnh, 
                                                                                std::string("rgb/image"),
                                                                                std::bind(&dai::rosBridge::ImageConverter::toRosMsg, 
                                                                                &rgbConverter,
                                                                                std::placeholders::_1, 
                                                                                std::placeholders::_2) , 
                                                                                queueSize,
                                                                                rgbCameraInfo,
                                                                                "rgb"                                                                             );

    rgbPublish.addPublisherCallback();

    if( enableDepth ){        
        auto stereoQueue = device.getOutputQueue("depth", queueSize, false);
        
        dai::rosBridge::ImageConverter* depthconverter = new dai::rosBridge::ImageConverter(full_tf_prefix + "_right_camera_optical_frame", false);            
        auto depthCameraInfo = depthconverter->calibrationToCameraInfo(calibrationHandler, dai::CameraBoardSocket::RIGHT, monoWidth, monoHeight);
        
        dai::rosBridge::BridgePublisher<sensor_msgs::Image, dai::ImgFrame>* depthPublish = new dai::rosBridge::BridgePublisher<sensor_msgs::Image, dai::ImgFrame>(stereoQueue,
                                                                                        pnh, 
                                                                                        std::string("depth/image"),
                                                                                        std::bind(&dai::rosBridge::ImageConverter::toRosMsg, 
                                                                                        depthconverter,                                                                                                    
                                                                                        std::placeholders::_1, 
                                                                                        std::placeholders::_2) , 
                                                                                        queueSize,
                                                                                        depthCameraInfo,
                                                                                        "depth");
        depthPublish->addPublisherCallback();
    }
                
    if( enableMono ){           
        dai::rosBridge::ImageConverter* leftconverter = new dai::rosBridge::ImageConverter(full_tf_prefix + "_left_camera_optical_frame", false);                
        auto leftQueue = device.getOutputQueue("left", queueSize, false);                        
        auto leftCameraInfo = leftconverter->calibrationToCameraInfo(calibrationHandler, dai::CameraBoardSocket::LEFT, monoWidth, monoHeight); 
        
        dai::rosBridge::BridgePublisher<sensor_msgs::Image, dai::ImgFrame> *leftPublish = new dai::rosBridge::BridgePublisher<sensor_msgs::Image, dai::ImgFrame>(leftQueue,
                                                                                        pnh, 
                                                                                        std::string("left/image"),
                                                                                        std::bind(&dai::rosBridge::ImageConverter::toRosMsg, 
                                                                                        leftconverter, 
                                                                                        std::placeholders::_1, 
                                                                                        std::placeholders::_2) , 
                                                                                        queueSize,
                                                                                        leftCameraInfo,
                                                                                        "left");

        leftPublish->addPublisherCallback();

        dai::rosBridge::ImageConverter* rightconverter = new dai::rosBridge::ImageConverter(full_tf_prefix + "_right_camera_optical_frame", true);
        auto rightQueue = device.getOutputQueue("right", queueSize, false);
        auto rightCameraInfo = rightconverter->calibrationToCameraInfo(calibrationHandler, dai::CameraBoardSocket::RIGHT, monoWidth, monoHeight);
     
        dai::rosBridge::BridgePublisher<sensor_msgs::Image, dai::ImgFrame> *rightPublish = new dai::rosBridge::BridgePublisher<sensor_msgs::Image, dai::ImgFrame>(rightQueue,
                                                                                        pnh, 
                                                                                        std::string("right/image"),
                                                                                        std::bind(&dai::rosBridge::ImageConverter::toRosMsg, 
                                                                                        rightconverter, 
                                                                                        std::placeholders::_1, 
                                                                                        std::placeholders::_2) , 
                                                                                        queueSize,
                                                                                        rightCameraInfo,
                                                                                        "right");

        rightPublish->addPublisherCallback();        
    }
    
    
    if( !NNblobPath.empty() ){
        
        // preview publisher
        auto previewQueue = device.getOutputQueue("preview", queueSize, false);
        dai::ros::ImageConverter* previewConverter = new dai::ros::ImageConverter(full_tf_prefix + "_rgb_camera_optical_frame", false);
        
        auto previewCameraInfo = previewConverter->calibrationToCameraInfo(calibrationHandler, dai::CameraBoardSocket::RGB, netInputWidth, netInputHeight); 
        
        dai::rosBridge::BridgePublisher<sensor_msgs::Image, dai::ImgFrame> *previewPublish = new dai::rosBridge::BridgePublisher<sensor_msgs::Image, dai::ImgFrame>(previewQueue,
                                                                                pnh, 
                                                                                std::string("net/preview/image"),
                                                                                std::bind(&dai::rosBridge::ImageConverter::toRosMsg, 
                                                                                previewConverter,
                                                                                std::placeholders::_1, 
                                                                                std::placeholders::_2), 
                                                                                queueSize,
                                                                                previewCameraInfo,
                                                                                "net/preview");

        previewPublish->addPublisherCallback();
        
        
        auto rawNNOutputQueue = device.getOutputQueue("nn_out_raw", queueSize, false);
        dai::ros::NNDataRawConverter* nnDataRawConverter = new dai::ros::NNDataRawConverter(netInputWidth, netInputHeight, full_tf_prefix + "_rgb_camera_optical_frame", false);
        
        dai::rosBridge::BridgePublisher<depthai_ros_extended_msgs::NeuralNetworkRawOutput, dai::NNData> *rawOutputPublish = new dai::rosBridge::BridgePublisher<depthai_ros_extended_msgs::NeuralNetworkRawOutput, dai::NNData>(rawNNOutputQueue,
            pnh,
            std::string("net/raw_output"),                                                                                                                                                                                                                    std::bind(&dai::ros::NNDataRawConverter::toRosMsg,                                                                                                                                                                                                                                                           nnDataRawConverter,                                                                                                                                                                                                                                                        std::placeholders::_1,                                                                                                                                                                                                                                                             std::placeholders::_2),                                                                                                                                                                                                                            queueSize                                                                                                                                                                                                                    
        );
        
        rawOutputPublish->addPublisherCallback();
        
    }
    
    if( enableIMU ){                
        
        auto imuQueue = device.getOutputQueue("imu", 30, false);
        
        dai::rosBridge::ImuConverter *imuConverter = new dai::rosBridge::ImuConverter(full_tf_prefix + "_imu_frame", dai::ros::ImuSyncMethod::LINEAR_INTERPOLATE_ACCEL, linearAccelCovariance, angularVelCovariance);
        
        dai::rosBridge::BridgePublisher<sensor_msgs::Imu, dai::IMUData> *imuPublish = new dai::rosBridge::BridgePublisher<sensor_msgs::Imu, dai::IMUData>(
            imuQueue,
            pnh,
            std::string("imu"),
            std::bind(&dai::rosBridge::ImuConverter::toRosMsg, imuConverter, std::placeholders::_1, std::placeholders::_2),
            30,
            "",
            "imu");

        imuPublish->addPublisherCallback();
        
    }
    
    ros::spin();
    return 0;
}
