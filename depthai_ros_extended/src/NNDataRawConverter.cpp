#include "NNDataRawConverter.h"

#include "depthai_bridge/depthaiUtility.hpp"


namespace dai {
namespace ros {
 
    NNDataRawConverter::NNDataRawConverter(int inputWidth, int inputHeight, std::string frame_id, bool getBaseDeviceTimestamp) :
    _steadyBaseTime(std::chrono::steady_clock::now())
    {
        inputHeight_ = inputHeight;
        inputWidth_ = inputWidth;
        frame_id_ = frame_id;        
        _rosBaseTime = ::ros::Time::now();
        _getBaseDeviceTimestamp = getBaseDeviceTimestamp;
    }
    
    void NNDataRawConverter::toRosMsg(std::shared_ptr<dai::NNData> inData, std::deque<depthai_ros_extended_msgs::NeuralNetworkRawOutput>& outMsgs){
        
        std::chrono::_V2::steady_clock::time_point tstamp;
        if( _getBaseDeviceTimestamp )
            tstamp = inData->getTimestampDevice();
        else
            tstamp = inData->getTimestamp();
    
        depthai_ros_extended_msgs::NeuralNetworkRawOutput nn_raw_output;
        //nn_raw_output.header.stamp = ros::Time::now();
        nn_raw_output.header.stamp = getFrameTime(_rosBaseTime, _steadyBaseTime, tstamp);
        nn_raw_output.header.frame_id = frame_id_;
        nn_raw_output.input_width = inputWidth_;
        nn_raw_output.input_height = inputHeight_;
        
        auto layer_names = inData->getAllLayerNames();
        for(const auto& name : layer_names){  
            dai::TensorInfo tensorInfo; // https://github.com/luxonis/depthai-shared/blob/main/include/depthai-shared/common/TensorInfo.hpp
            inData->getLayer(name, tensorInfo);
            
            depthai_ros_extended_msgs::LayerRawOutput layer_raw;
            layer_raw.name = name;
            for( size_t i = 0 ; i < tensorInfo.numDimensions ; i++){
                std_msgs::MultiArrayDimension dim;
                dim.size = tensorInfo.dims[i];
                dim.stride = tensorInfo.strides[i];
                int order = ((int)tensorInfo.order >> ((tensorInfo.numDimensions - i - 1)*4)) & 0xF;
                if( order == (int)dai::TensorInfo::StorageOrder::W )
                    dim.label = "width";
                else if( order == (int)dai::TensorInfo::StorageOrder::H )
                    dim.label = "height";
                else if( order == (int)dai::TensorInfo::StorageOrder::C )
                    dim.label = "channel";
                else
                    dim.label = "batch";
                layer_raw.tensor.layout.dim.push_back(dim);                        
            }
            layer_raw.tensor.layout.data_offset = tensorInfo.offset;//not sure if needed
            if( tensorInfo.dataType == dai::TensorInfo::DataType::FP16){                                                
                layer_raw.tensor.data = inData->getLayerFp16(name);
            }
            else{
                ROS_WARN("Unsupported datatype %i in layer %s, layer will be skipped", tensorInfo.dataType, name.c_str());                        
                continue;
            }        
            nn_raw_output.layers.push_back(layer_raw);
        }
        outMsgs.push_back(nn_raw_output);
    }
    
}
}
