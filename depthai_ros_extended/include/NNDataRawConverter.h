#pragma once

#include <deque>
#include "depthai/depthai.hpp"
#include "depthai_ros_extended_msgs/NeuralNetworkRawOutput.h"
#include "depthai_ros_extended_msgs/LayerRawOutput.h"
#include "std_msgs/MultiArrayDimension.h"
#include "ros/time.h"

namespace dai {
namespace ros {
 
    class NNDataRawConverter{
    public:
        NNDataRawConverter(int inputWidth, int inputHeight, std::string frame_id, bool getBaseDeviceTimestamp);
        
        void toRosMsg(std::shared_ptr<dai::NNData> inData, std::deque<depthai_ros_extended_msgs::NeuralNetworkRawOutput>& outMsgs);
        
    private:
        int inputWidth_, inputHeight_;
        std::string frame_id_;
        ::ros::Time _rosBaseTime;
        bool _getBaseDeviceTimestamp;
        std::chrono::time_point<std::chrono::steady_clock> _steadyBaseTime;
    };
        
}
}
